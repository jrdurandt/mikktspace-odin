# MikkTSpace bindings for Odin

[MikkTSpace](https://github.com/mmikk/MikkTSpace) bindings for [Odin]()

MikkTSpace algorithm used to generate tangent and normal maps. Implemented by Morten S. Mikkelsen. Used in many great tools such as Blender and Godot. See [here](http://www.mikktspace.com/) for more information.

> mikktspace used under MIT License, included in source files

## TODO:
- [] Testing!
- [] Implement simpler interface

## Build
Only linux builds have been defined

`cd src`   
`make`
